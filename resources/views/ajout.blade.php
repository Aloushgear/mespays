@extends('layouts.main')
@section('content')
<div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Veuillez remplir les champs ci-dessous</h4>
          <p class="card-category">Complete your profile</p>
        </div>
        <div class="card-body">
          <form action="{{route ('pays.store')}}" method="POST">
            @method("POST")
            @csrf
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <label class="bmd-label-floating">Libellé </label>
                  <input type="text" class="form-control" name="libelle" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="bmd-label-floating">Description</label>
                  <input type="text" class="form-control" name="description" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="bmd-label-floating">Code_indicatif</label>
                  <input type="text" class="form-control" name="code_indicatif" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Continent</label>
          
                  <!--input type="select" class="form-control" name="#" required-->
                  <select class="form-control" name="continent" required>
                    <option value="Afrique">Afrique</option>
                    <option value="Europe">Europe</option>
                    <option value="Asie">Amerique</option>
                    <option value="Oceanie">Oceanie</option>
                    <option value="Antartique">Antartique</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Population</label>
                  <input type="number" class="form-control" name="population" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="bmd-label-floating">Capitale</label>
                  <input type="text" class="form-control" name="capital" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="bmd-label-floating">Monnaie</label>
                  <!--input type="text" class="form-control" name="monnaie"required-->
                  <select class="form-control" name="monnaie"required>
                    <option value="XOF">XOF</option>
                    <option value="EUR">EUR</option>
                    <option value="DOLLAR">DOLLAR</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="bmd-label-floating">Langue</label>
                  <!--input type="text" class="form-control" name="langue" required-->
                  <select class="form-control" name="langue" required>
                    <option value="FR">FR</option>
                    <option value="EN">EN</option>
                    <option value="AR">AR</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="bmd-label-floating">Superficie</label>
                  <input type="number" class="form-control" name="superficie" >
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="bmd-label-floating">Est_laique</label><br>
                  <label>Cocher si c'est vrai cochez la première case dans le cas contraire la suivante</label>
                  <input type="checkbox" class="form-control" name="est_laique" value="1" ><br>
                  <input type="checkbox" class="form-control" name="est_laique" value="0" >
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <!--label>About Me</label-->
                  <div class="form-group">
                    <!--label class="bmd-label-floating"> Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</label-->
                    <textarea class="form-control" rows="5"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary pull-right">Ajouter information</button>
            <div class="clearfix"></div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-profile">
        <div class="card-avatar">
          <a href="javascript:;">
            <img class="img" src="../assets/img/faces/rasb.jpg" />
          </a>
        </div>
        <div class="card-body">
          <h6 class="card-category text-gray">Hebergeur de la page</h6>
          <h4 class="card-title">Git Personnal</h4>
          <p class="card-description">
            Pas d'information à afficher
          </p>
          <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
        </div>
      </div>
    </div>
  </div>
@endsection
