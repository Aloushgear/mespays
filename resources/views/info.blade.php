@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
            <h4 class="card-title ">Liste des pays</h4>
            <a href="{{url('/ajout')}}"><button class="btn btn-success">Ajouter des pays</button></a>
            <p class="card-category"></p>

            </div>
            <div class="card-body">

            <div class="table-responsive">
                <table class="table">

                <thead class=" text-primary">

                    <th>
                    ID
                    </th>
                    <th>
                    Libelle
                    </th>
                    <th>
                    Description
                    </th>
                    <th>
                    Code_indicatif
                    </th>
                    <th>
                    Continent
                    </th>
                    <th>
                    Population
                    </th>
                    <th>
                    Capitale
                    </th>
                    <th>
                    Monnaie
                    </th>
                    <th>
                    Langue
                    </th>
                    <th>
                    Superficie
                    </th>
                    <th>
                    Est_laique
                    </th>
                </thead>
                <tbody>
                    @foreach ($pays as $produit)
              <tr>
                  <td>{{$produit->id}}</td>
                  <td>{{$produit->libelle}}</td>
                  <td>{{$produit->description}}</td>
                  <td>{{$produit->code_indicatif}}</td>
                  <td>{{$produit->continent}}</td>
                  <td>{{$produit->population}}</td>
                  <td>{{$produit->capitale}}</td>
                  <td>{{$produit->monnaie}}</td>
                  <td>{{$produit->langue}}</td>
                  <td>{{$produit->superficie}}</td>
                  <td>{{$produit->est_laique}}</td>
                </tr>
              @endforeach

                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
@endsection

