<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo"><a href="{{url('/main')}}" class="simple-text logo-normal">
        Home
      </a></div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item active  ">
          <a class="nav-link" href="{{url('/info')}}">
            <i class="material-icons">liste des cients</i>
            <p>Liste des Pays</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{url('/ajout')}}">
            <i class="material-icons">flag</i>
            <p>Ajouter des pays</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
