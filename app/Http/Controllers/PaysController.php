<?php

namespace App\Http\Controllers;

use App\Models\Pay;
use Illuminate\Http\Request;

class PaysController extends Controller
{
    //
    public function info()
    {
        return view('info', [
            "pays" => Pay::all()
        ]);
    }
    public function ajout()
    {
        return view('ajout');
    }

    public function store(Request $request)
    {
        //dd($request);
        //validation de la requête
        $request->validate([
            "libelle" => 'required'
        ]);
        Pay::create([
            "libelle" => $request->get('libelle'),
            "description" => $request->get('description'),
            "code_indicatif" => $request->get('code_indicatif'),
            "continent" => $request->get('continent'),
            "population" => $request->get('population'),
            "capitale" => $request->get('capitale'),
            "monnaie" => $request->get('monnaie'),
            "langue" => $request->get('langue'),
            "superficie" => $request->get('superficie'),
            "est_laique" => $request->get('est_laique')
        ]);

        return redirect()->route("pays.info");
    }
}
