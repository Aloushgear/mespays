<?php

namespace Database\Factories;

use App\Models\Pay;
use Illuminate\Database\Eloquent\Factories\Factory;

class PayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pay::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'libelle'=>$this->faker->title,
            'description'=>$this->faker->sentence,
            'code_indicatif'=>$this->faker->postcode,
            'continent'=>$this->faker->randomElement(['Afrique','Europe','Asie','Amerique','Oceanie','Antartique']),
            'population'=>$this->faker->randomElement([145546,879887,646545,46545,78945,4564]),
            'capitale'=>$this->faker->state,
            'monnaie'=>$this->faker->randomElement(['XOF','EUR','DOLLAR']),
            'langue'=>$this->faker->randomElement(['FR','EN','AR','ES']),
            'superficie'=>$this->faker->randomElement([448498987,546546544,65456465,464654646,313687]),
            'est_laique'=>$this->faker->boolean,
        ];
    }
}
